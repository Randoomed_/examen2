package com.ucbcba.book.controllers;

import com.ucbcba.book.entities.Author;
import com.ucbcba.book.entities.Book;
import com.ucbcba.book.entities.BookCategory;
import com.ucbcba.book.services.AuthorService;
import com.ucbcba.book.services.BookCategoryService;
import com.ucbcba.book.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class BookController {
    BookService bookService;
    BookCategoryService bookCategoryService;
    AuthorService authorService;


    @Autowired
    public void setBookService(BookService bookService){
        this.bookService=bookService;
    }

    @Autowired
    public void setBookCategoryService(BookCategoryService bookCategoryService){
        this.bookCategoryService=bookCategoryService;
    }

    @Autowired
    public void setAuthorService(AuthorService authorService){
        this.authorService=authorService;
    }

    @RequestMapping(value = "/books", method = RequestMethod.GET)
    public String index(Model model){
        List<Book> books = (List) bookService.listAllBooks();
        model.addAttribute("books",books);
        return "books";
    }

    /*@RequestMapping(value = "/books/showall", method = RequestMethod.GET)
    public String showall(Model model){
        List<Book> books = (List) bookService.listAllBooks();
        model.addAttribute("books",books);
        return "books";
    }*/

    @RequestMapping(value="/book/edit/{id}",method = RequestMethod.GET)
    public String edit(@PathVariable Integer id, Model model){
        Book book = bookService.findBook(id);
        List<BookCategory> bookCategories =
                (List) bookCategoryService.listAllBookCategories();
        List<Author> authors=
                (List) authorService.listAllAuthors();
        model.addAttribute("book", book);
        model.addAttribute("bookCategories", bookCategories);
        model.addAttribute("authors", authors);
        return "editBook";
    }

    @RequestMapping(value = "/book/{id}", method = RequestMethod.GET)
    public String show(@PathVariable Integer id, Model model){
        Book book = bookService.findBook(id);
        model.addAttribute("book", book);
        return "show";
    }

    @RequestMapping(value = "/book/new", method = RequestMethod.GET)
    public String newBook(Model model){
        List<BookCategory> bookCategories =
                (List) bookCategoryService.listAllBookCategories();
        List<Author> authors=
                (List) authorService.listAllAuthors();
        model.addAttribute("book",new Book());
        model.addAttribute("bookCategories", bookCategories);
        model.addAttribute("authors",authors);
        return "newBook";
    }

    @RequestMapping(value="/book", method = RequestMethod.POST)
    public String create(@ModelAttribute("book") @Valid Book book,Model model){
        System.out.println("Book category " + book.getBookCategory().getId());
        System.out.println("Book author" + book.getAuthor().getId());
        bookService.saveBook(book);
        return "redirect:/books";
    }

    @RequestMapping(value= "/book/delete/{id}",method = RequestMethod.GET)
    public String delete(@PathVariable Integer id, Model model){
        bookService.deleteBook(id);
        return "redirect:/books";
    }

    @RequestMapping(value= "/book/like/{id}",method = RequestMethod.GET)
    public String like(@PathVariable Integer id, Model model){
        Book book;
        book=bookService.findBook(id);
        book.setLikes(book.getLikes()+1);
        bookService.saveBook(book);
        return "redirect:/books";
    }


    @RequestMapping(value= "/book/dislike/{id}",method = RequestMethod.GET)
    public String dislike(@PathVariable Integer id, Model model){
        Book book;
        book=bookService.findBook(id);
        book.setLikes(book.getLikes()-1);
        if(book.getLikes()>=0){
            bookService.saveBook(book);
        }
        return "redirect:/books";
    }




    //Authors
    @RequestMapping(value = "/authors", method = RequestMethod.GET)
    public String indexAuthor(Model model){
        List<Author> authors = (List) authorService.listAllAuthors();
        model.addAttribute("authors",authors);
        return "authors";
    }

    @RequestMapping(value="/author/edit/{id}",method = RequestMethod.GET)
    public String editAuthor(@PathVariable Integer id, Model model){
        Author author = authorService.findAuthor(id);
        model.addAttribute("author", author);
        return "editAuthor";
    }

    @RequestMapping(value = "/author/{id}", method = RequestMethod.GET)
    public String showAuthors(@PathVariable Integer id, Model model){
        Author author = authorService.findAuthor(id);
        model.addAttribute("author", author);
        return "showAuthor";
    }

    @RequestMapping(value = "/author/new", method = RequestMethod.GET)
    public String newAuthor(Model model){
        model.addAttribute("author",new Author());
        return "newAuthor";
    }

    @RequestMapping(value="/author", method = RequestMethod.POST)
    public String createAuthor(@ModelAttribute("author") Author author,Model model){
        authorService.saveAuthor(author);
        return "redirect:/authors";
    }

    @RequestMapping(value= "/author/delete/{id}",method = RequestMethod.GET)
    public String deleteAuthor(@PathVariable Integer id, Model model) {
        authorService.deleteAuthor(id);
        return "redirect:/authors";
    }
}
