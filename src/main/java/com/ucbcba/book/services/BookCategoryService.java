package com.ucbcba.book.services;

import com.ucbcba.book.entities.BookCategory;

public interface BookCategoryService {
    Iterable<BookCategory> listAllBookCategories();
}
