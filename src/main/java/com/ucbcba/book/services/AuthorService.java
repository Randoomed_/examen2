package com.ucbcba.book.services;

import com.ucbcba.book.entities.Author;

public interface AuthorService {
    Iterable<Author>listAllAuthors();
    Author findAuthor(Integer id);
    void saveAuthor(Author author);
    void deleteAuthor(Integer id);
}
