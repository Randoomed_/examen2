package com.ucbcba.book.repositories;

import com.ucbcba.book.entities.Author;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface AuthorRepository extends CrudRepository<Author, Integer> {
}
